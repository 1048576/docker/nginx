#!/usr/bin/env sh

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

# Chown
_sh "chown -R www-data:www-data ./"
_sh "chown -R www-data:www-data /etc/nginx/"
_sh "chown -R www-data:www-data /home/nginx/"
_sh "chown -R www-data:www-data /var/lib/nginx/"

# Clean
_sh "rm -rf /build.d/"
